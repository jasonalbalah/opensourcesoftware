/*
Note: read over and adapt how you need! 
Uncomment print statements for algorithm details...
@author, jason.albalah@yahoo.com
*/

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


    vector<int> ar;
/*
Wikipedia: Quicksort - Lomuto partition scheme
algorithm quicksort(A, lo, hi) is
    if lo < hi then
        p := partition(A, lo, hi)
        quicksort(A, lo, p � 1)
        quicksort(A, p + 1, hi)
algorithm partition(A, lo, hi) is
    pivot := A[hi]
    i := lo        // place for swapping
    for j := lo to hi � 1 do
        if A[j] = pivot then
            swap A[i] with A[j]
            i := i + 1
    swap A[i] with A[hi]
    return i
*/
void printAr(){
    for(int c = 0; c < ar.size(); c++){
        printf("%d ", ar.at(c));
    } printf("\n");
}
int partition(int lo, int hi){
    //printAr();
    int p = ar.at(hi);
    int i = lo, j, t;
    for(j = lo; j < hi; j++){
        if(ar.at(j) <= p){ 
            //printAr();
            //printf("if %d < %d\n", ar.at(i), p);
            //ar.at(i) = arr.at(j);
            //printf("swap %d with %d\n", ar.at(i), ar.at(j));
            t = ar[i];
            ar[i] = ar[j];
            ar[j] = t;
            i += 1;
        }
    }
    t = ar[i];
    ar[i] = ar[j];
    ar[j] = t;
    //printf("set %d to %d\n", ar.at(i), ar.at(j));
    printAr();
    return i;
}
void quicksort(int lo, int hi){      
    int p;
    //printAr();
    if(lo < hi){
        //printAr(); 
        p = partition(lo, hi); 
        quicksort(lo, p - 1);
        quicksort(p + 1, hi);
        //printAr();
     }
}

int main() {
    printf("\nThis program implements quicksort using in-place array manipulation.\n");
    printf(" - reducing memory overhead for large arrays.\n\n");
    printf("Type the length length of array, then press enter: ");
    int size;
    int n;
    cin >> size;
    printf("Now, enter array of numbers (space-separated or 1-by-one): ");
    for(int ch = 0; ch < size; ch++){
        cin >> (n);
        ar.push_back(n);
        //printf("%d ",ar.at(ch));
    }
    quicksort(0, ar.size()-1); 
    return 0;
}